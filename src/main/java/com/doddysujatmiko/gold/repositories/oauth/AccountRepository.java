package com.doddysujatmiko.gold.repositories.oauth;

import com.doddysujatmiko.gold.entities.oauth.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long>, JpaSpecificationExecutor<Account> {
    Optional<Account> findAccountByUsername(String username);

    @Query("FROM Account u WHERE u.otp = ?1")
    Account findOneByOTP(String otp);

    @Query("FROM Account u WHERE LOWER(u.username) = LOWER(:username)")
    Account checkExistingEmail(String username);

    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}
