package com.doddysujatmiko.gold.repositories.oauth;

import com.doddysujatmiko.gold.entities.oauth.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, JpaSpecificationExecutor<Role> {
    Role findRoleByName(String name);
    List<Role> findByNameIn(String[] names);
}
