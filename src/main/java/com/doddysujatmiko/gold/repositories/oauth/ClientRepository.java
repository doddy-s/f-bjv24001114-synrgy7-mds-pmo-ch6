package com.doddysujatmiko.gold.repositories.oauth;


import com.doddysujatmiko.gold.entities.oauth.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>, JpaSpecificationExecutor<Client> {
    Client findClientByClientId(String clientId);
}
