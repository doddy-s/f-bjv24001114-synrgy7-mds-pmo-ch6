package com.doddysujatmiko.gold.dto.oauth;

import lombok.Data;

@Data
public class LoginRes {
    private Object accessToken;
    private Object tokenType;
    private Object refreshToken;
    private Object expiresIn;
    private Object scope;
    private Object jti;
}
