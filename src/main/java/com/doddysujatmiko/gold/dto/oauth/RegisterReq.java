package com.doddysujatmiko.gold.dto.oauth;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class RegisterReq {
    @NotEmpty(message = "must not empty")
    private String username;

    @NotEmpty(message = "must not empty")
    private String email;

    @NotEmpty(message = "must not empty")
    private String password;
}
