package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "products")
public class Product extends AbstractEntity implements Serializable {
    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @JsonIgnore
    @ManyToOne(targetEntity = Merchant.class)
    @JoinColumn(name="merchant_id", referencedColumnName = "id")
    private Merchant merchant;

    @JsonIgnore
    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetails;
}
