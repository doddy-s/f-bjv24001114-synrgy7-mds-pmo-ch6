package com.doddysujatmiko.gold.entities.oauth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(
        name = "oauth_role",
        uniqueConstraints = @UniqueConstraint(
                name = "role_name_and_type",
                columnNames = {"type", "name"}
        )
)
@Getter
@Setter
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",length = 20)
    private String name;

    private String type;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RolePath> rolePaths;

    @JsonIgnore
    @ManyToMany(targetEntity = Account.class, mappedBy = "roles", fetch = FetchType.LAZY)
    private List<Account> accounts;


    @Override
    @JsonIgnore
    public String getAuthority() {
        return this.name;
    }
}
