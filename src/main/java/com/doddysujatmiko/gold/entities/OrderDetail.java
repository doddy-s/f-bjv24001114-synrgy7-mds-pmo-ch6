package com.doddysujatmiko.gold.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "order_details")
public class OrderDetail extends AbstractEntity implements Serializable {
    @Column(name = "quantity")
    private Integer quantity;

    @JsonIgnore
    @Column(name = "total_price")
    private Integer totalPrice;

    @JsonIgnore
    @ManyToOne(targetEntity = Product.class, cascade = CascadeType.ALL)
    @JoinColumn(name="product_id", referencedColumnName = "id")
    private Product product;

    @JsonIgnore
    @ManyToOne(targetEntity = Order.class, cascade = CascadeType.ALL)
    @JoinColumn(name="order_id", referencedColumnName = "id")
    private Order order;
}
