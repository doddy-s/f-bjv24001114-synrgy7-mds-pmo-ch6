package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.User;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.UUID;

public interface UserService {
//    ResponseEntity<Map<Object, Object>> login(User user);
//
//    ResponseEntity<Map<Object, Object>> register(User user);

    ResponseEntity<Map<Object, Object>> update(User user);

    ResponseEntity<Map<Object, Object>> read(UUID id);

    ResponseEntity<Map<Object, Object>> delete(UUID id);

    ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String userUsername);
}
