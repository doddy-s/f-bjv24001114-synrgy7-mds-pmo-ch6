package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.dto.oauth.LoginReq;
import com.doddysujatmiko.gold.dto.oauth.RegisterReq;
import com.doddysujatmiko.gold.entities.oauth.Account;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface AuthService {
    public Account register(RegisterReq request);
    public ResponseEntity<Map<Object, Object>> login(LoginReq request);
}
