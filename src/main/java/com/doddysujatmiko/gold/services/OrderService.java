package com.doddysujatmiko.gold.services;

import com.doddysujatmiko.gold.entities.Order;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.UUID;

public interface OrderService {
    ResponseEntity<Map<Object, Object>> create(Order order);

    ResponseEntity<Map<Object, Object>> read(UUID id);

    ResponseEntity<Map<Object, Object>> update(Order order);

    ResponseEntity<Map<Object, Object>> addItem(UUID orderId, UUID orderDetailId);

    ResponseEntity<Map<Object, Object>> delete(UUID id);

    ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String destinationAddress, UUID userId);
}
