package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.User;
import com.doddysujatmiko.gold.exceptions.BadRequestException;
import com.doddysujatmiko.gold.exceptions.NotFoundException;
import com.doddysujatmiko.gold.repositories.UserRepository;
import com.doddysujatmiko.gold.services.UserService;
import com.doddysujatmiko.gold.utils.ErrorHandler;
import com.doddysujatmiko.gold.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

//    @Override
//    public ResponseEntity<Map<Object, Object>> login(User user) {
//        try {
//            Optional<User> userOptional = userRepository.findByEmail(user.getEmail());
//
//            if(userOptional.isEmpty()) {
//                throw new NotFoundException("Email not found");
//            }
//
//            if(!Objects.equals(userOptional.get().getPassword(), user.getPassword())) {
//                throw new BadRequestException("Wrong password");
//            }
//
//            return Responser.constructSuccess(userOptional.get(), "Success", HttpStatus.OK);
//        } catch (Throwable t) {
//            return ErrorHandler.handleError(t);
//        }
//    }
//
//    @Override
//    public ResponseEntity<Map<Object, Object>> register(User user) {
//        try {
//            if(user.getUsername().isEmpty()) {
//                throw new BadRequestException("Username can not be empty");
//            }
//
//            if(user.getEmail().isEmpty()) {
//                throw new BadRequestException("Email can not be empty");
//            }
//
//            if(user.getPassword().isEmpty()) {
//                throw new BadRequestException("Password can not be empty");
//            }
//
//            User createdUser = userRepository.save(user);
//
//            return Responser.constructSuccess(createdUser, "User Created", HttpStatus.CREATED);
//        } catch (Throwable t) {
//            return ErrorHandler.handleError(t);
//        }
//    }

    @Override
    public ResponseEntity<Map<Object, Object>> update(User user) {
        try {
            if(user.getId() == null) {
                throw new BadRequestException("User Id can not be empty");
            }

            Optional<User> exUser = userRepository.findById(user.getId());

            if(exUser.isEmpty()) {
                throw new NotFoundException("User not found");
            }

            User updatedUser = userRepository.save(user);

            return Responser.constructSuccess(updatedUser, "User Updated", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> read(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("User Id can not be empty");
            }

            Optional<User> userOptional = userRepository.findById(id);

            if(userOptional.isEmpty()) {
                throw new NotFoundException("User not found");
            }

            return Responser.constructSuccess(userOptional.get(), "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> delete(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("User Id can not be empty");
            }

            Optional<User> userOptional = userRepository.findById(id);

            if(userOptional.isEmpty()) {
                throw new NotFoundException("User not found");
            }

            userRepository.deleteById(id);

            return Responser.constructSuccess("", "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String userUsername) {
        try {
            if(page == null) {
                page = 0;
            }

            if(size == null) {
                size = 10;
            }

            Pageable pageable = PageRequest.of(page, size);

            Specification<User> spec = (((root, query, criteriaBuilder) -> {
                List<Predicate> predicates = new ArrayList<>();
                if(userUsername != null) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("username")), "%" + userUsername.toLowerCase() + "%"));
                }
                return criteriaBuilder.and(predicates.toArray(predicates.toArray(new Predicate[0])));
            }));

            Page<User> userList = userRepository.findAll(spec, pageable);

            return Responser.constructSuccess(userList, "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }
}
