package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.dto.oauth.LoginReq;
import com.doddysujatmiko.gold.dto.oauth.LoginRes;
import com.doddysujatmiko.gold.dto.oauth.RegisterReq;
import com.doddysujatmiko.gold.entities.User;
import com.doddysujatmiko.gold.entities.oauth.Account;
import com.doddysujatmiko.gold.entities.oauth.Role;
import com.doddysujatmiko.gold.exceptions.BadRequestException;
import com.doddysujatmiko.gold.exceptions.NotFoundException;
import com.doddysujatmiko.gold.repositories.oauth.AccountRepository;
import com.doddysujatmiko.gold.repositories.oauth.RoleRepository;
import com.doddysujatmiko.gold.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Value("${BASEURL}")
    private String baseUrl;

    @Autowired
    private PasswordEncoder encoder;

    public Account register(RegisterReq request) {
        String[] roleNames = {"ROLE_USER", "ROLE_READ", "ROLE_WRITE"}; // admin

        if (accountRepository.existsByUsername(request.getUsername())) {
            throw new BadRequestException("Username already exist");
        }

        if (accountRepository.existsByEmail(request.getEmail())) {
            throw new BadRequestException("Email already exist");
        }

        Account account = new Account();
        account.setUsername(request.getUsername().toLowerCase());
        account.setEmail(request.getEmail());
        account.setPassword(encoder.encode(request.getPassword()));
        List<Role> r = roleRepository.findByNameIn(roleNames);
        account.setRoles(r);

        return accountRepository.save(account);
    }

    public ResponseEntity<Map<Object, Object>> login(LoginReq request) {
        Optional<Account> account = accountRepository.findAccountByUsername(request.getUsername());

        if (account.isEmpty()) {
            throw new NotFoundException("User not found");
        }

        if (!encoder.matches(request.getPassword(), account.get().getPassword())) {
            throw new BadRequestException("Wrong password");
        }

//        if (account.get().isEnabled()) {
//            throw new BadRequestException("Account is not enabled");
//        }

        String url = baseUrl + "/oauth/token?username=" + request.getUsername() +
                "&password=" + request.getPassword() +
                "&grant_type=password" +
                "&client_id=my-client-web" +
                "&client_secret=password";
        ResponseEntity<Map<Object, Object>> response = restTemplateBuilder.build().exchange(url, HttpMethod.POST, null, new
                ParameterizedTypeReference<Map<Object, Object>>() {
                }
        );

        if (response.getStatusCode() == HttpStatus.OK) {
            return response;
        } else {
            throw new NotFoundException("Account not found");
        }
    }
}
