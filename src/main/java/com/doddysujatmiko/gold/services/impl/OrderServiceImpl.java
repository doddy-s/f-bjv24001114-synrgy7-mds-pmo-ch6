package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.*;
import com.doddysujatmiko.gold.exceptions.BadRequestException;
import com.doddysujatmiko.gold.exceptions.NotFoundException;
import com.doddysujatmiko.gold.repositories.OrderDetailRepository;
import com.doddysujatmiko.gold.repositories.OrderRepository;
import com.doddysujatmiko.gold.repositories.UserRepository;
import com.doddysujatmiko.gold.services.OrderService;
import com.doddysujatmiko.gold.utils.ErrorHandler;
import com.doddysujatmiko.gold.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Override
    public ResponseEntity<Map<Object, Object>> create(Order order) {
        try {
            if(order.getDestinationAddress().isEmpty()) {
                throw new BadRequestException("Destination address can not be empty");
            }
            order.setCompleted(false);
            Optional<User> user = userRepository.findById(order.getUser().getId());

            if(user.isEmpty()) {
                throw new NotFoundException("Merchant not found");
            }

            order.setUser(user.get());

            Order createdOrder = orderRepository.save(order);

            return Responser.constructSuccess(createdOrder, "Order Created", HttpStatus.CREATED);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> read(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("Order Id can not be empty");
            }

            Optional<Order> orderOptional = orderRepository.findById(id);

            if(orderOptional.isEmpty()) {
                throw new NotFoundException("Order not found");
            }

            return Responser.constructSuccess(orderOptional.get(), "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> update(Order order) {
        try {
            if(order.getId() == null) {
                throw new BadRequestException("Order Id can not be empty");
            }

            Optional<Order> exOrder = orderRepository.findById(order.getId());

            if(exOrder.isEmpty()) {
                throw new NotFoundException("Order not found");
            }

            Order updatedOrder = orderRepository.save(order);

            return Responser.constructSuccess(updatedOrder, "Order Updated", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> addItem(UUID orderId, UUID orderDetailId) {
        try {
            if(orderId == null) {
                throw new BadRequestException("Order Id can not be empty");
            }

            Optional<Order> exOrder = orderRepository.findById(orderId);

            if(exOrder.isEmpty()) {
                throw new NotFoundException("Order not found");
            }

            Optional<OrderDetail> exOrderDetail = orderDetailRepository.findById(orderDetailId);

            if(exOrderDetail.isEmpty()) {
                throw new NotFoundException("Order detail not found");
            }

            Order order = exOrder.get();
            OrderDetail orderDetail = exOrderDetail.get();

            orderDetail.setOrder(order);
            order.getOrderDetails().add(orderDetail);

            Order updatedOrder = orderRepository.save(order);
            orderDetailRepository.save(orderDetail);

            return Responser.constructSuccess(updatedOrder, "Order Updated", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> delete(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("Order Id can not be empty");
            }

            Optional<Order> orderOptional = orderRepository.findById(id);

            if(orderOptional.isEmpty()) {
                throw new NotFoundException("Order not found");
            }

            orderRepository.deleteById(id);

            return Responser.constructSuccess("", "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String destinationAddress, UUID userId) {
        try {
            if(page == null) {
                page = 0;
            }

            if(size == null) {
                size = 10;
            }

            Pageable pageable = PageRequest.of(page, size);

            Specification<Order> spec = (((root, query, criteriaBuilder) -> {
                List<Predicate> predicates = new ArrayList<>();
                if(destinationAddress != null) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + destinationAddress.toLowerCase() + "%"));
                }
                if (userId != null) {
                    Join<Product, Merchant> merchantJoin = root.join("merchant");
                    predicates.add(criteriaBuilder.equal(merchantJoin.get("id"), userId));
                }
                return criteriaBuilder.and(predicates.toArray(predicates.toArray(new Predicate[0])));
            }));

            Page<Order> orderList = orderRepository.findAll(spec, pageable);

            return Responser.constructSuccess(orderList, "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }
}
