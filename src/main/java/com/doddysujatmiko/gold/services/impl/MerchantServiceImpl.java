package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.Merchant;
import com.doddysujatmiko.gold.exceptions.BadRequestException;
import com.doddysujatmiko.gold.exceptions.NotFoundException;
import com.doddysujatmiko.gold.repositories.MerchantRepository;
import com.doddysujatmiko.gold.services.MerchantService;
import com.doddysujatmiko.gold.utils.ErrorHandler;
import com.doddysujatmiko.gold.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.*;

@Service
public class MerchantServiceImpl implements MerchantService {
    @Autowired
    private MerchantRepository merchantRepository;

    @Override
    public ResponseEntity<Map<Object, Object>> create(Merchant merchant) {
        try {
            if(merchant.getName().isEmpty()) {
                throw new BadRequestException("Merchant name can not be empty");
            }

            if(merchant.getLocation().isEmpty()) {
                throw new BadRequestException("Location can not be empty");
            }

            if(merchant.getOpen() == null) {
                merchant.setOpen(false);
            }

            Merchant createdMerchant = merchantRepository.save(merchant);

            return Responser.constructSuccess(createdMerchant, "Merchant Created", HttpStatus.CREATED);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> read(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("Merchant Id can not be empty");
            }

            Optional<Merchant> merchantOptional = merchantRepository.findById(id);

            if(merchantOptional.isEmpty()) {
                throw new NotFoundException("Merchant not found");
            }

            return Responser.constructSuccess(merchantOptional.get(), "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> update(Merchant merchant) {
        try {
            if(merchant.getId() == null) {
                throw new BadRequestException("Merchant Id can not be empty");
            }

            Optional<Merchant> exMerchant = merchantRepository.findById(merchant.getId());

            if(exMerchant.isEmpty()) {
                throw new NotFoundException("Merchant not found");
            }

            Merchant updatedMerchant = merchantRepository.save(merchant);

            return Responser.constructSuccess(updatedMerchant, "Merchant Updated", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> delete(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("Merchant Id can not be empty");
            }

            Optional<Merchant> merchantOptional = merchantRepository.findById(id);

            if(merchantOptional.isEmpty()) {
                throw new NotFoundException("Merchant not found");
            }

            merchantRepository.deleteById(id);

            return Responser.constructSuccess("", "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String merchantName) {
        try {
            if(page == null) {
                page = 0;
            }

            if(size == null) {
                size = 10;
            }

            Pageable pageable = PageRequest.of(page, size);

            Specification<Merchant> spec = (((root, query, criteriaBuilder) -> {
                List<Predicate> predicates = new ArrayList<>();
                if(merchantName != null) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + merchantName.toLowerCase() + "%"));
                }
                return criteriaBuilder.and(predicates.toArray(predicates.toArray(new Predicate[0])));
            }));

            Page<Merchant> merchantList = merchantRepository.findAll(spec, pageable);

            return Responser.constructSuccess(merchantList, "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }
}
