package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.OrderDetail;
import com.doddysujatmiko.gold.entities.Product;
import com.doddysujatmiko.gold.exceptions.BadRequestException;
import com.doddysujatmiko.gold.exceptions.NotFoundException;
import com.doddysujatmiko.gold.repositories.OrderDetailRepository;
import com.doddysujatmiko.gold.repositories.ProductRepository;
import com.doddysujatmiko.gold.services.OrderDetailService;
import com.doddysujatmiko.gold.utils.ErrorHandler;
import com.doddysujatmiko.gold.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.*;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {
    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public ResponseEntity<Map<Object, Object>> create(OrderDetail orderDetail) {
        try {
            if(orderDetail.getQuantity() == null) {
                throw new BadRequestException("OrderDetail quantity can not be empty");
            }

            Optional<Product> product = productRepository.findById(orderDetail.getProduct().getId());

            if(product.isEmpty()) {
                throw new NotFoundException("Product not found");
            }

            orderDetail.setProduct(product.get());

            orderDetail.setTotalPrice(product.get().getPrice() * orderDetail.getQuantity());

            OrderDetail createdOrderDetail = orderDetailRepository.save(orderDetail);

            return Responser.constructSuccess(createdOrderDetail, "OrderDetail Created", HttpStatus.CREATED);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> read(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("OrderDetail Id can not be empty");
            }

            Optional<OrderDetail> orderDetailOptional = orderDetailRepository.findById(id);

            if(orderDetailOptional.isEmpty()) {
                throw new NotFoundException("OrderDetail not found");
            }

            return Responser.constructSuccess(orderDetailOptional.get(), "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> update(OrderDetail orderDetail) {
        try {
            if(orderDetail.getId() == null) {
                throw new BadRequestException("OrderDetail Id can not be empty");
            }

            Optional<OrderDetail> exOrderDetail = orderDetailRepository.findById(orderDetail.getId());

            if(exOrderDetail.isEmpty()) {
                throw new NotFoundException("OrderDetail not found");
            }

            OrderDetail updatedOrderDetail = orderDetailRepository.save(orderDetail);

            return Responser.constructSuccess(updatedOrderDetail, "OrderDetail Updated", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> delete(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("OrderDetail Id can not be empty");
            }

            Optional<OrderDetail> orderDetailOptional = orderDetailRepository.findById(id);

            if(orderDetailOptional.isEmpty()) {
                throw new NotFoundException("OrderDetail not found");
            }

            orderDetailRepository.deleteById(id);

            return Responser.constructSuccess("", "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, UUID productId) {
        try {
            if(page == null) {
                page = 0;
            }

            if(size == null) {
                size = 10;
            }

            Pageable pageable = PageRequest.of(page, size);

            Specification<OrderDetail> spec = (((root, query, criteriaBuilder) -> {
                List<Predicate> predicates = new ArrayList<>();
                if (productId != null) {
                    Join<OrderDetail, Product> productJoin = root.join("product");
                    predicates.add(criteriaBuilder.equal(productJoin.get("id"), productId));
                }
                return criteriaBuilder.and(predicates.toArray(predicates.toArray(new Predicate[0])));
            }));

            Page<OrderDetail> orderDetailList = orderDetailRepository.findAll(spec, pageable);

            return Responser.constructSuccess(orderDetailList, "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }
}
