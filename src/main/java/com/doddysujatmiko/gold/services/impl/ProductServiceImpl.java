package com.doddysujatmiko.gold.services.impl;

import com.doddysujatmiko.gold.entities.Merchant;
import com.doddysujatmiko.gold.entities.Product;
import com.doddysujatmiko.gold.exceptions.BadRequestException;
import com.doddysujatmiko.gold.exceptions.NotFoundException;
import com.doddysujatmiko.gold.repositories.MerchantRepository;
import com.doddysujatmiko.gold.repositories.ProductRepository;
import com.doddysujatmiko.gold.services.ProductService;
import com.doddysujatmiko.gold.utils.ErrorHandler;
import com.doddysujatmiko.gold.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MerchantRepository merchantRepository;

    @Override
    public ResponseEntity<Map<Object, Object>> create(Product product) {
        try {
            if(product.getName().isEmpty()) {
                throw new BadRequestException("Product name can not be empty");
            }

            if(product.getPrice() == null) {
                throw new BadRequestException("Price can not be empty");
            }

            Optional<Merchant> merchant = merchantRepository.findById(product.getMerchant().getId());

            if(merchant.isEmpty()) {
                throw new NotFoundException("Merchant not found");
            }

            product.setMerchant(merchant.get());

            Product createdProduct = productRepository.save(product);

            return Responser.constructSuccess(createdProduct, "Product Created", HttpStatus.CREATED);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> read(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("Product Id can not be empty");
            }

            Optional<Product> productOptional = productRepository.findById(id);

            if(productOptional.isEmpty()) {
                throw new NotFoundException("Product not found");
            }

            return Responser.constructSuccess(productOptional.get(), "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> update(Product product) {
        try {
            if(product.getId() == null) {
                throw new BadRequestException("Product Id can not be empty");
            }

            Optional<Product> exProduct = productRepository.findById(product.getId());

            if(exProduct.isEmpty()) {
                throw new NotFoundException("Product not found");
            }

            Product updatedProduct = productRepository.save(product);

            return Responser.constructSuccess(updatedProduct, "Product Updated", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> delete(UUID id) {
        try {
            if(id == null) {
                throw new BadRequestException("Product Id can not be empty");
            }

            Optional<Product> productOptional = productRepository.findById(id);

            if(productOptional.isEmpty()) {
                throw new NotFoundException("Product not found");
            }

            productRepository.deleteById(id);

            return Responser.constructSuccess("", "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }

    @Override
    public ResponseEntity<Map<Object, Object>> readFilteredPage(Integer page, Integer size, String productName, UUID merchantId) {
        try {
            if(page == null) {
                page = 0;
            }

            if(size == null) {
                size = 10;
            }

            Pageable pageable = PageRequest.of(page, size);

            Specification<Product> spec = (((root, query, criteriaBuilder) -> {
                List<Predicate> predicates = new ArrayList<>();
                if(productName != null) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + productName.toLowerCase() + "%"));
                }
                if (merchantId != null) {
                    Join<Product, Merchant> merchantJoin = root.join("merchant");
                    predicates.add(criteriaBuilder.equal(merchantJoin.get("id"), merchantId));
                }
                return criteriaBuilder.and(predicates.toArray(predicates.toArray(new Predicate[0])));
            }));

            Page<Product> productList = productRepository.findAll(spec, pageable);

            return Responser.constructSuccess(productList, "Success", HttpStatus.OK);
        } catch (Throwable t) {
            return ErrorHandler.handleError(t);
        }
    }
}
