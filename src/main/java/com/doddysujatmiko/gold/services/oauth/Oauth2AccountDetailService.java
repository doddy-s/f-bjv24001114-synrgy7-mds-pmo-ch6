package com.doddysujatmiko.gold.services.oauth;

import com.doddysujatmiko.gold.entities.oauth.Account;
import com.doddysujatmiko.gold.repositories.oauth.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Oauth2AccountDetailService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Account> account = accountRepository.findAccountByUsername(s);
        if (account.isEmpty()) {
            throw new UsernameNotFoundException(String.format("Username %s is not found", s));
        }

        return account.get();
    }

}