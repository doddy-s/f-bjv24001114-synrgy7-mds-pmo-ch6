package com.doddysujatmiko.gold.exceptions;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String s) {
        super(s);
    }
}
